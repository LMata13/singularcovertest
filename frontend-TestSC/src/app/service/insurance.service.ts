import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Insurance } from '../model/insurance.model';

@Injectable({
  providedIn: 'root'
})
export class InsuranceService {
  jsonData = 'assets/InsurProducts.json';

  constructor(private http: HttpClient) { }

  getInsurances(): Observable<any> {
    return this.http.get<Insurance[]>(this.jsonData);
  }

  /*getInsuranceById(id: number) {
    return this.http.get<Insurance>(id);
  }

  deleteInsurance(id: number) {
    return this.http.delete(id);
  }*/
}

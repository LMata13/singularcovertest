import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { InsuranceService } from '../../../service/insurance.service';
import { Insurance } from '../../../model/insurance.model';
import { Config } from 'protractor/built/config';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.sass']
})
export class InsuranceComponent implements OnInit {
  insur: Insurance;

  public searchTxt = '';
  show = true;

  constructor(private http: HttpClient, private iService: InsuranceService) { }

  ngOnInit() {
    this.getData();
  }

  // Get data from Json
  getData() {
    this.iService.getInsurances().subscribe( data => {
      this.insur = data;
      console.log(data);
    },
    err => {
      console.log('Error getting data: ' + err);
    });
  }

}

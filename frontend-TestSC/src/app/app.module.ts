import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from 'angular-6-datatable';
import { FormsModule } from '@angular/forms';

// Modules
import { AppRoutingModule } from './app-routing.module';

// Services
import { InsuranceService } from './service/insurance.service';

// Components
import { AppComponent } from './app.component';
import { ModulesComponent } from './modules/modules.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { FooterComponent } from './modules/footer/footer.component';
import { NavigationComponent } from './modules/navigation/navigation.component';
import { InsuranceComponent } from './modules/dashboard/insurance/insurance.component';
import { SearchPipe } from './modules/dashboard/insurance/searchPipe';

@NgModule({
  declarations: [
    AppComponent,
    ModulesComponent,
    DashboardComponent,
    FooterComponent,
    NavigationComponent,
    InsuranceComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    DataTableModule,
    FormsModule
  ],
  providers: [InsuranceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
